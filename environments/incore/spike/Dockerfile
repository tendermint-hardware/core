# Shakti / InCore Development Environment Image
FROM debian:10

# SET ENVIRONMENT VARIABLES
# This allows the user to actualy invoke all of the commands that we compile in this docker image, and tells the build processes where they can find the riscv toolchain.
ENV PATH=$PATH:/opt/riscv/bin
ENV RISCV=/opt/riscv
ENV PATH=$PATH:/opt/riscv/bin
ENV PATH=$PATH:$RISCV/bin:$RISCV/riscv32/bin:$RISCV/riscv64/bin
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RISCV/riscv32/lib:$RISCV/riscv64/lib
ENV PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/bin
ENV PATH=$PATH:$RISCV/riscv32/riscv32-unknown-elf/lib
ENV PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/bin
ENV PATH=$PATH:$RISCV/riscv64/riscv64-unknown-elf/lib

# UPDATE / UPGRADE
# This step ensures that the docker image is fully up to date and is ready for dependency installation.
RUN apt update && \
	apt -y dist-upgrade

# ALL DEPENDENCIES
# This step installs all of the dependencies needed to build the tooling necessary to build a Shakti or Chromite Core.
RUN apt install -y flex bison gperf git make libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl xz-utils tk-dev  liblzma-dev python-openssl g++ autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev gawk build-essential texinfo libtool patchutils bc zlib1g-dev device-tree-compiler pkg-config libexpat-dev libusb-1.0

# MOD-SPIKE
# Using directions from this repository: https://gitlab.com/shaktiproject/tools/mod-spike/-/tree/bump-to-latest
RUN git clone https://gitlab.com/shaktiproject/tools/mod-spike.git && \
    cd mod-spike && \
    git checkout bump-to-latest && \
    git clone https://github.com/riscv/riscv-isa-sim.git && \
    cd riscv-isa-sim && \
    git checkout 6d15c93fd75db322981fe58ea1db13035e0f7add && \
    git apply ../shakti.patch && \
    mkdir build && \
    cd build && \
    ../configure --prefix=$RISCV && \
    make -j $(nproc) 1>/dev/null && \
    make install

# RISC V OPENOCD
# This now installs a patched version of OpenOCD
RUN wget https://gitlab.com/incoresemi/utils/common_utils/-/raw/master/chromite/openocd.patch && \
	git clone https://github.com/riscv/riscv-openocd && \
	cd riscv-openocd && \
	git checkout 95a8cd9b5d07501ac2243c61f331b092b1ea9894 && \
	git apply ../openocd.patch && \
	./bootstrap && \
	./configure --enable-jlink --enable-remote-bitbang --enable-jtag_vpi --enable-ftdi --prefix=$RISCV && \
	make -j $(nproc) 1>/dev/null && \
	make install

# DEVICE TREE COMPILER
RUN cd ~/ && \
	wget https://git.kernel.org/pub/scm/utils/dtc/dtc.git/snapshot/dtc-1.4.7.tar.gz && \
	tar -xvzf dtc-1.4.7.tar.gz && \
	cd dtc-1.4.7/ && \
	make NO_PYTHON=1 PREFIX=/usr/ 1>/dev/null && \
	make install NO_PYTHON=1 PREFIX=/usr/
