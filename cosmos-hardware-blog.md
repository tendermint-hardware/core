In January, 2020, I made a [forum post](https://forum.virgo.org/t/interested-in-joining-the-virgo-team/54/2)


That one... Which led to a number of things like phone calls and such, and after a short time, led to working on hardware via Virgo Cosmos. 
And that...... Folks it's been really interesting. 
The initial target was basically a raspi with an open source core. 
I dug into this and learned that mainly, shipped RISC-V CPUs aren't open source. 
They instead implement the open source RISC-V ISA and bundle proprietary core IP into the package and don't release the resulting full core design.
Much more research followed, and I learned that there were variations on this theme, but that shipped cores tended to be closed.
Along the way my friend Shyam told me that I should look into the Shakti project from IIT Madras. 
I looked into it, and ended up talking with Madhu, the lead of Shakti at IIT Madras and CEO of InCore Semiconductors.
Once for an hour and a second time....for three and a half hours.
During that 3.5 hour call he took me on a journey through the future of computing, where hardware is specalized for software, and the whole stack is open source from top to bottom. 
.....he also shared with me that we could have a core fabbed for $25-35k, and that the C Class / Chromite design had already been manufactured as ASICs before.
I was very enthusiastic but did not know where to begin.
A week or two later I figured out `how to start`.
Found the right docs and with no small amount of difficulty, set up the 14GB dev env correctly and got a core working and tested in simulation.
But it was non-deterministic
Not the core, but the development environmentAnd even getting to that point-- without the funding I got from Jae Kwon, there's simply no way that I could have found the amount of time it took to get that single, non-deterministic C Class / Chromite development environment running. 
I bet the first attempt took 40-60 hours. 
......but the core built and passed tests.  Quest accomplished. 
I figured that I could improve on the docs and reduce developer time spend by a large amount by making a dev env setup [script](https://gitlab.com/tendermint-hardware/core/-/blob/master/debian10.sh). that ran on an empty Debian 10 machine and got the env rolling.


This is a project to enable other developers to modify an open source spec for a CPU, build a simulator and run tests.
I was right, it initally took the env setup time down to 6 hours without parallelizing c/c++ builds.  Progress.
And it was deterministic-ish.  An empty Debian 10 machine is an empty Debian 10 machine. 
Developers could have a known-good setup.
Build times were still pretty much insane, but it was better.
But I wanted to do the whole thing automatically, any time the repo changed.  And I wanted the dev env, which has frequent upstream changes from many projects, to always be fresh.
So:

We could think of this functionally.  That dev env produces a docker image, which is consumed by the core builder, which produces cores in three flavors: sim, fpga and asic.
Another repo, builds Linux.  When cores are built, Linux will be tested against them. 
Most of this is now automated though I am still working on full FPGA support.
Once we have an automated system that tests mainline Linux against a freshly compiled core's verilog in an FPGA:
At that time....
We are ready to physically build an open source system on a chip at a foundry.

Along the way, we reduced dev env setup time for new Shakti/Chromite developers from days to weeks down to "as fast as you can download 14GB"

And put a very solid beginning on a multiproject build pipeline to further reduce dev time burden.
All of this stuff got upstreamed into the InCore Semiconductors repositories and Shakti repositories. 

Where might this go?
There are many open source CPU core designs, but few make it to manufacturing without the inclusion of proprietary IP, which taints the core from a security and auditability perspective. 
With small tweaks, this pipeline works for nearly all of them as they're mainly built in Bluespec.
So we are going to make it >10x easier and faster for devs to play with these cores, in fact they no longer need a local build environment.  Any commit on a branch of that repo will trigger a build against a pretty beefy cluster using GitLab CI, and that build uses ccache and is highly parallelized. 
Basically we can offer hackers and hobbyists nearly what they'd get in terms of automation at a semiconductor design firm. 
The manufacturing pipeline that we are going to pioneer and open source.... Works for nearly all of them, as well.

So with the magic of GitLab CI and the help of some open source friends, we should be able to create a system/framework for open source semiconductor design and commercialization.