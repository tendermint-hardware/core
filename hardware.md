#  Tendermint Hardware
Hardware is a sovereign, blockchain mediated organization  that coordinates the development and production of fully open source hardware, beginning with a RISC-V CPU core, and evolving over time into complete products like single board computers, routers, mobile phones, and laptops.

This vision is enormous, and will take time, particularly if care is taken to ensure that products do not include closed components.  

We will ship 100% Open Source (emphasis on auditability) minimal products that have clear demand and use cases as rapidly as possible.

There is wide consensus that these fully open, auditable Hardware products are due to the presence of well known security issues in existing consumer and commercial grade hardware products.

Hardware is not a company.

Hardware may work with a "spender company" that allows it to pay for services in traditional fiat currencies.  

Hardware is a blockchain mediated organization that coordinates a global push toward fully open hardware.  

Hardware ensures transparency and accountability throughout the entire development, manufacturing, and commercialization process.  Hardware's transparency increases the security of products created or commercialized in its design pipeline. Furthermore, Hardware is able to 
 

## Components

* Blockchain Community
* Build Acceleration
    * Continuous Integration
    * Build Cluster
    * Deterministic Development Environments



## Community
Hardware is validated by organizations and individuals who are deeply convicted that:

1) Open Hardware benefits humanity
2) Open Software is insecure without open hardware
3) Commercialization -> Volume -> Quality and Reduced Prices
4) Auditability yeilds security

### Companies
* InCore Semiconductors (need to ask)
* Tendermint, Inc (need to define)
* PCBViet (need to define)
* Privex

### Individuals
* Shyam Siani


## Milestones
- Ship a Chromite or C Class CPU to manufacturing by January 15, 2021
- Formalize the Build Cluster by September 1, 2020


## Blazing Fast Automated builds for the community

Our main bottleneck right now is build times; but from what I can tell we can reduce build times by adding CPU cores, so that's how I chose that particular CPU.

There are other options, 


## Accomplished so far

* Learned the build process
* Upgraded the 


## Finances
Hardware is fully transparent.

Here's the financial rundown so far:

- Jacob Gadikian has a $50/hr contract with Tendermint, inc
  - Jacob has been paid .71 BTC by Jae Kwon

- Jacob Gadikian pays for a build cluster at Hetzner.de which is shared with Blurt:
  - 2 A61X-NVMe @ 84 EUR/mo
  - 1 A51X-NVMe @ 51 EUR/mo